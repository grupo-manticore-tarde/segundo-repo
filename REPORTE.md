# REPORTES

## Roles
* [General](#general)
* [Guest](#guest)
* [Reporter](#reporter)
* [Developer](#developer)
* [Maintainer](#maintainer)

### Guest 

* **ERROR** al realizar un push 

* **ERROR** al realizar un merge

* **ERROR** al realizar un cambio de rama

* **ERROR** el realizar un merge request

* **ERROR** al eliminar una rama

* **ERROR** al realizar merge a la master

* **ERROR** al realizar merge a entre ramas

* **ERROR** al generar cualquier componente de CI/CD

* **ERROR** asignar un issues

* **ADMITE** crear un issues 

* **ADMITE** clonar el repositorio

* **ADMITE** cambiar entre ramas 

* **ADMITE** realizar commit en ramas localmente 

* **ADMITE** realizar un pull 

* **SUGERENCIAS** en las wikis 

* **ACCESO DENEGADO** a las configuraciones del proyecto

IR a [Roles](#roles)


### Reporter

* **ERROR** al eliminar una rama

* **ERROR** al realizar push a la master

* **ERROR** al realizar push a entre ramas

* **ERROR** al realizar merge a la master

* **ERROR** al realizar merge a entre ramas

* **ERROR** al generar cualquier componente de CI/CD

* **ERROR** el realizar un merge request

* **ERROR** al realizar un push

* **ADMITE** crear un issues

* **ADMITE** asignar un issues

* **ADMITE** realizar un pull

* **ADMITE** clonar el repositorio

* **ADMITE** cambiar entre ramas

* **SUGERENCIAS** en las wikis 

* **ACCESO DENEGADO** a las configuraciones del proyecto

IR a [Roles](#roles)


### Developer

* **ERROR** al realizar push a la master 

* **ERROR** al eliminar una rama

* **ADMITE** realizar un pull

* **ADMITE** crear un Issue

* **ADMITE** asignar un issues

* **ADMITE** clonar el repositorio

* **ADMITE** realizar un merge request

* **ADMITE** crear una nueva rama

* **ADMITE** realizar push entre ramas las que contenga el proyecto incluyendo las del mismo desarrollador

* **ADMITE** cambiar entre ramas 

* **ADMITE** al generar cualquier componente de CI/CD

* **ADMITE** crear páginas en la wiki 

* **ACCESO DENEGADO** a las configuraciones del proyecto

IR a [Roles](#roles)


### Maintainer

* **ERROR** al eliminar una rama

* **ADMITE** asignar un issues

* **ADMITE** realizar un Issue

* **ADMITE** clonar el repositorio

* **ADMITE** realizar un merge request

* **ADMITE** crear una nueva rama

* **ADMITE** realizar push entre ramas

* **ADMITE** realizar un pull

* **ADMITE** cambiar entre ramas 

* **ADMITE** realizar push entre ramas las que contenga el proyecto incluyendo las del mismo desarrollador

* **ADMITE** realizar push a la master

* **ADMITE** realizar merge a la master

* **ADMITE** realizar merge a entre ramas

* **ADMITE** al generar cualquier componente de CI/CD

* **ADMITE** crear páginas en la wiki 

* **ACCESO** a las configuraciones del proyecto

IR a [Roles](#roles)

## GENERAL

Roles | Permisos | Restricciones
------|---------- | ------------
guest, reporter, developer, mantainer | Editar Issues, Cerrar Issues, Hacer pull | Eliminar Issues, Eliminar Ramas

IR a [Roles](#roles)