#  Kevin Jimenez

## Descripcion
Tengo 23 años,  actualmente me encuentro estudiando en la [Escuela Politecnica Nacional](https://www.epn.edu.ec/) en la carrera de [Ingeniera en sistemas de informacion y de Computacion](https://fis.epn.edu.ec/).

Actualmente me encuentro viviendo en **[Quito](https://www.google.com.ec/maps/place/Quito/data=!4m2!3m1!1s0x91d59a4002427c9f:0x44b991e158ef5572?sa=X&ved=2ahUKEwiK46v1g_PcAhUjwVkKHchGBFUQ8gEwAHoECAAQAQ)**

Somos 4 mienbros de mi familia, mis padres y mi hermana la cual tiene 16 años.

Me gradue del **Colegio Juar Pio Montufar** de **Fisico - Matematico**
Adicionalmente me gusta la programacion, la seguridad informatica e incluso la inteligencia Artificial.
Actualmente me encuentro en 8vo semnestre.
Ademas tengo 2 tatuajes situados en el brazo izquierdo, me gusta ver series especial mente los simpsons, futurama.

![simpsons](https://i.pinimg.com/564x/43/74/32/437432095cf5fbfeb1939343d1b513b5.jpg)
![futurama](https://i.pinimg.com/564x/16/2e/89/162e89bed1bf7a85281de2396cf459ec.jpg)


Las peliculas que mas me gustan son la de accion y ciencia ficcion entre ellas las de comics que son de DC

![DC Comics](https://i.pinimg.com/236x/a6/5c/07/a65c078d71ebd93d2190c5014f380d92.jpg)




## Redes Sociales
[Facebook](https://www.facebook.com/Kev24.orlando/about?lst=100002186211379%3A100002186211379%3A1534472145)

[Googel +](https://plus.google.com/u/0/104533543358721404148)
## Hobbies
### jugar futbol

![soccer](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTifX3m_wKMJaXyVsgKVJrC6kvspZzRILnAI5KHYTflQ3GN3kL6)


### jugar play

![ps4](https://wallup.net/wp-content/uploads/2017/11/23/425096-PSP-play-PlayStation-joystick-PlayStation_4-748x487.jpg)

## Musica Favorita

#### Mi estilo de musica es la siguiente

#### si quieren una prueba de las musica dar clic en la imagen

##### Rap

[![Eminen](https://pbs.twimg.com/profile_images/929030268043845633/ilS1ri2v_400x400.jpg)](https://www.youtube.com/watch?v=_Yhyp-_hX2s)

##### Hip-Hop

[![Movimiento Original](https://i.pinimg.com/564x/f3/7c/cd/f37ccdadb119dd690da158b7395e7272.jpg)](https://www.youtube.com/watch?v=I4dy7MTUP0A)

##### Rock Clasico

[![Guns and Roses](https://e.an.amtv.pe/espectaculos-guns-n-roses-lima-conoce-todos-detalles-concierto-esta-noche-n252274-624x352-317541.jpg)](https://www.youtube.com/watch?v=Rbm6GXllBiw)

##### Electronica

[![Avicii](https://i.pinimg.com/564x/4a/33/92/4a339269d857c71c42cf882b3949856f.jpg)](https://www.youtube.com/watch?v=tuTUC63kjDY)

## Equipos Favoritos

### Ac Milan

![ac milan](https://i.pinimg.com/564x/76/7e/38/767e38b18e7845a889c92a4d5ad08a51.jpg)

### Barcelona SC

![Barcelona SC](https://i.pinimg.com/564x/c4/9f/59/c49f5984986e75b3c62087767436ef19.jpg)

### Manchester United

![Manchester United](https://i.pinimg.com/236x/a4/7e/bc/a47ebcbd3ba8d011018c5c4ea356f00e.jpg)

### Borussia Dortmund

![Borussia Dortmund](https://i.pinimg.com/236x/cc/ab/af/ccabaf8a0c8e3354030b6a40d90d464a.jpg)
